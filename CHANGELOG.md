# [0.2.0]

## Feature

+ 升级仓颉版本 `0.58.3`

## Bugfix

+ 暂无

## Remove

+ 暂无

# [0.1.0]

## Feature

+ 添加字节数组和输入流的判断支持

## Bugfix

+ 暂无

## Remove

+ 暂无
