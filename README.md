<div align="center">
<h1>is-png-cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.2.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.59.6-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-100.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-Cloud-brightgreen" style="display: inline-block;" />
</p>

## 介绍

is_png_cj 提供了判断文件或字节流是否为图片的功能。

### 项目特性

- 支持对流和字节数组进行判断。
- 使用魔数进行判断。

### 项目计划

1. 2024 年 9 月发布 0.1.0 版本。
2. 对项目进行后续维护。

## 项目架构

本项目提供了 `isPng` 函数判断文件是否为 PNG 图片。

### 源码目录

```shell
.
├── README.md             # 整体介绍
├── examples              # 示例项目目录
└── src                   # 源码目录
    ├── util.cj           # 封装了文件判断的工具代码
    ├── is_png_test.cj    # 单元测试
    └── is_png.cj         # 核心代码
```

### 接口说明

```cangjie
/**
 * 判断输入流是否为 PNG 文件
 * @param inputStream 待判断的输入流。执行后该函数会对传入输入流进行读取，需要注意处理。
 * @return 如果是 PNG 文件则返回 true，否则返回 false
 */
public func isPng(inputStream: InputStream): Bool

/**
 * 判断输入流是否为 PNG 文件
 * @param iter 待判断的 Iterable 实例
 * @return 如果是 PNG 文件则返回 true，否则返回 false
 */
public func isPng(iter: Iterable<Byte>): Bool 

```

## 使用说明
### 引入依赖

在 `cjpm.toml` 中的 `[dependencies]` 内添加如下内容：

```toml
is_png_cj = { git = "https://gitcode.com/Cangjie-TPC/is-png-cj" }
```

添加后 `cjpm.toml` 如下所示：

```toml
[dependencies]
  is_png_cj = { git = "https://gitcode.com/PermissionDog/is-png-cj" }

[package]
  cjc-version = "0.58.3"
  compile-option = ""
  description = "nothing here"
  link-option = ""
  name = "your_package_name_here"
  output-type = "executable"
  src-dir = ""
  target-dir = ""
  version = "1.0.0"
  package-configuration = {}
```

### 功能示例

#### 判断文件是否为 PNG 功能示例

功能示例描述:

读取一个本地文件，并判断其是否为 PNG 图片。

示例代码如下：
```cangjie
import is_png
import std.io.*
import std.fs.*

main() {
    let f = File("test.png", Open(true, false))
    println(if (isPng(f)) {
        "该文件是 PNG 格式"
    } else {
        "该文件不是 PNG 格式"
    })
    f.close()
}
```

执行结果如下：

```
该文件是 PNG 格式
```

#### 判断字节数组是否为 PNG 功能示例

功能示例描述:

从 Base64 编码获取一个字节数组，并判断其是否为 PNG 图片。

示例代码如下：

```cangjie
import is_png
import std.io.*
import encoding.base64.*

main() {
    let image = fromBase64String(
        "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAANSURBVBhXY/i5cct/AAhjA162NVWRAAAAAElFTkSuQmCC"
    ).getOrThrow()
    println(if (isPng(image)) {
        "该文件是 PNG 格式"
    } else {
        "该文件不是 PNG 格式"
    })
}
```

执行结果如下：

```
该文件是 PNG 格式
```

## 约束与限制

无

## 开源协议
[MIT License](LICENSE)

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。

本项目committer：[@PermissionDog](https://gitcode.com/PermissionDog)

本项目由 [SIGCANGJIE / 仓颉兴趣组](https://gitcode.com/SIGCANGJIE) 实现并维护。技术支持和意见反馈请提Issue。

This project is supervised by [@zhangyin-gitcode](https://gitcode.com/zhangyin_gitcode).