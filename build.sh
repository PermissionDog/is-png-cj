#!/bin/bash

# 检查是否有输入参数
if [ "$#" -eq 0 ]; then
    echo "Usage: $0 [build|test|cov]"
    exit 1
fi

case "$1" in
    build)
        echo "Building project..."
        cjpm build
        ;;
    test)
        echo "Executing testing..."
        cjpm test --coverage --report-path=reports --report-format=xml
        ;;
    cov)
        echo "Executing coverage report generation, please ensure that the test has been run"
        cjcov --root=./ -i src -e src/*_test.cj --html-details -o reports/cov
        ;;
    *)
        echo "Invalid argument."
        echo "Usage: $0 [build|test|cov]"
        exit 1
        ;;
esac