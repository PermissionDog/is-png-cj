package is_png_cj

import std.io.*

private let MAGIC_NUMBER_PNG: Pattern = [
    ByteMatch(0x89), ByteMatch(0x50), ByteMatch(0x4E), ByteMatch(0x47),
    ByteMatch(0x0D), ByteMatch(0x0A), ByteMatch(0x1A), ByteMatch(0x0A)
]

/**
 * 判断输入流是否为 PNG 文件
 * @param inputStream 待判断的输入流。执行后该函数会对传入输入流进行读取，需要注意处理。
 * @return 如果是 PNG 文件则返回 true，否则返回 false
 */
public func isPng(inputStream: InputStream): Bool {
    return startsWith(inputStream, MAGIC_NUMBER_PNG)
}

/**
 * 判断输入流是否为 PNG 文件
 * @param iter 待判断的 Iterable 实例
 * @return 如果是 PNG 文件则返回 true，否则返回 false
 */
public func isPng(iter: Iterable<Byte>): Bool {
    return startsWith(iter, MAGIC_NUMBER_PNG)
}
